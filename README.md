# ÚKOL #

1. vytvořit acc na [bitbucket.org](https://bitbucket.org/account/signup/)
2. stáhnout [sourcetree](http://www.sourcetreeapp.com/) (win 7+) nebo [msysgit](https://msysgit.github.io/) (win xp+)
3. fork repozitáře [2C](https://bitbucket.org/sos-jh/2c) - tlačítko **Fork** na BB
4. clone svého forku na localhost
5. přidat textový soubor `jmeno.prijmeni.txt`, do něj vložit url svého BB profilu
6. commit & push
7. vytvořit pull request do zdrojového repozitáře - tlačítko **Create pull request** na BB

### Markdown ###
toto readme je psané ve formátovacím jazyce [Markdown](http://lifehacky.cz/oda-na-markdown-co-to-vlastne-je-a-proc-se-bez-nej-neobejdete) ([syntax](https://bitbucket.org/tutorials/markdowndemo/overview))